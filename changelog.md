### v1.2

- Reformatted the output to make it a little more friendly
- Added some sanity code to ignore empty lines in backup file 

### v1.1

- Moved to new repo (Bye XDA - i will no longer link my work from a place where mods protect people who mock people with disabilities (a pattern sadly))

- Adding back update.json and change to module.prop as necessary

### v1.0 

- First release
