# Superuser List Restore

A simple Magisk Module to restore Magisks Superuser list from an existing backup file

*This is a module requested by XDA member @christantoan, who quite rightly asked why, if i had backup and restore modules for MagiskHide/Deny Lists, why wasnt there the same functionality for the Superuser list.*

After explaining that id thought of it plenty of times, but due to my overwhelming fondness for laziness, put it in the Much Later basket, i set about trying to make his Xmas Wish come true...

**PLEASE NOTE:** 

- We (myself, Osm0sis, ipdev and pndwal) all tried to get this backup and restore functionality (along with backup/restore for the magiskhide/deny list - i have modules for that too - see here: [Related Modules](#related-modules)) added natively to Magisk Manager via Magisk's Github, lets just say it was rejected, so here we are...

- These/There are **TWO** very discinct modules that work together, one backs up ([SuperuserListBackup](https://gitlab.com/adrian.m.miller/superuserlistbackup)),
and one restores ([SuperuserListRestore](https://gitlab.com/adrian.m.miller/superuserlistrestore)). You need **BOTH** modules. 

---

### How It Works: ###

This module:

- Loads the backup file from the partner SuperuserListBackup module (/storage/emulated/0/SuperUserList.txt)
- Queries the /data/system/packages.list using the package name from the backup
- Extract the packages matching UID and formats it
- Inserts the UID into magisk.db's policies table with common options (permanent grant, logging and notification)
- Loops until all packages UIDs are in the magisk.db's policies table

**If you havent yet created your backup, please see the [SuperuserListBackup Module Repo](https://gitlab.com/adrian.m.miller/superuserlistbackup) for the partner module to create your backup**

---

### Module Installation: ###

- Download from **[Releases](https://gitlab.com/adrian.m.miller/superuserlistrestore/-/releases)**  
![](https://gitlab.com/adrian.m.miller/superuserlistrestore/-/badges/release.svg)
- Install the module via Magisk app/Fox Magisk Module Manager/MRepo
- Reboot

---

### Usage: ###

- Make sure /sdcard/SuperUserList.txt exists and contains the packages you want to give su to

- Make sure some/all the apps in /sdcard/SuperUserList.txt are present/installed (or restored from backup on new ROM flash - which is what i use this as part of)

- Install SuperuserListRestore module

  This:
      - Loads the backup file from the partner SuperuserListBackup module (/storage/emulated/0/SuperUserList.txt)
      - Queries the /data/system/packages.list using the package name from the backup
      - Extract the packages matching UID and formats it
      - Inserts the UID into magisk.db's policies table with common options (permanent grant, logging and notification)
      - Loops until all packages UIDs are in the magisk.db's policies table
 
- Reboot is NOT necessary

The module will create a logfile (/storage/emulated/0/SuperUserListRestore.log) on install, which mirrors the information onscreen. If you have any issues, you'll need to start by looking there, and by opening an issue on this repo's Issues

The only active file in the entire module is /common/install.sh, and it is commented.

The module will remain installed, unless removed, after the process completes.

It is safe to leave installed and ignored if you like.

---

### Changelog ###

Please see: https://gitlab.com/adrian.m.miller/superuserlistrestore/-/blob/main/changelog.md

---

### Related Modules: ###

MagiskHideDenyBackup: https://gitlab.com/adrian.m.miller/magiskhidedenybackup

MagiskHideDenyRestore: https://gitlab.com/adrian.m.miller/magiskhidedenyrestore

---
